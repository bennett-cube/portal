# Contribution Guidelines
​
We need
  * [Code Contributions](#code-contributions)
​
​
## Code Contributions
​
This is a Python3/Django project.
​
### How to start
​
  * Clone the repository and `cd` into it:
    ```
    git clone git@gitlab.com:corona-help.org/portal.git corona-help-portal && cd corona-help-portal/
    ```
  * Create a Python3 virtual environment and activate it:
    ```
    virtualenv -p python3 venv && . ./venv/bin/activate
    ```
  * Install all required packages:
    ```
    pip install -r requirements.txt
    ```
  * Prepare and start application locally:
    ```
    python manage.py migrate
    python manage.py createsuperuser
    python manage.py runserver
    ```
    Now you can access the application here: http://localhost:8000/
​
### Start Developing
​
  * Load the Python3 virtual environment:
    ```
    . ./venv/bin/activate
    ```
  * Start the server:
    ```
    python manage.py runserver
    ```
​
### Some words to code quality
​
We need to be fast, but we still need to be able to read other's code.
For that reason, your code should be [PEP8](https://www.python.org/dev/peps/pep-0008/) compliant
(except maximum line length of 120 characters).
Also, it shouldn't have syntax errors.

You can use `python -m flake8` for running test cases and PEP8 checks.


### Submit your work
​
If you are part of the development team:
  * Create a feature/bugfix branch from master
    ```
    git checkout -b feature/cool-things
    ```
  * If you're done, create a Merge Request and get your code merged to the `master` branch!
    Technically, you're allowed to push to the `master` branch directly, which might be a better option for small improvements.
    Bigger changed should be peer-reviewed, in this case Merge Requests should be the preferred way to do.
​
If you are an external contributor:
  * Fork the project.
  * If you're done, create a Merge Request and get your code merged to the `master` branch!
  * Thank you so much for participating!