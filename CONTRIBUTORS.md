# Contributors

These are our active project members and contributors:

  * **Dorian Cantzen** (Extrument): Project Lead, Backend Development
  * **Matthias Lohr**: Infrastructure/DevOps
  * **Ozgu Karaca**: Frontend Development
  * **Lars Trautmann**, **Markus Dittrich**, **Toni Harzer**, **Janina Blum** (http://www.upstruct.com): Brand Design, UI and UX Design
