import React from 'react';
import LandingPage from './landingPage';
import CookieConsent from './cookieConsent';
import Footer from './footer';
import '../styles/reset.scss';

const App = () => {
  return (
    <div className="App">
      <LandingPage />
      <Footer />
      <CookieConsent />
    </div>
  );
}

export default App;
