import React from "react";
import { useTranslation } from "react-i18next";
import { Phone, Info } from "react-feather";
import Logo from "../../styles/images/logo.svg"
import './styles.scss';

const Header = () => {
    const { t } = useTranslation();

    return (
        <nav className="navbar">
            <div className="navbar-leftpart">
                <a className="navbar-logo" href="#">
                    <img src={Logo} width="auto" height="40" alt="logo" />
                </a>
                <button className="navbar-hotline btn-hotline">
                    <Phone size="20" />
                    <a href="tel:066268099862" className="hotline-tel"><span>  {t('header.hotline')} <span className="tel"> coming soon </span></span> </a>
                    <Info size="20" />
                </button>
            </div>
            <div className="navbar-buttons">
                <button type="button" className="btn btn-login">{t('header.login')}</button>
                <button type="button" className="btn btn-helper">{t('header.register')} </button>
            </div>
        </nav>
    );
}

export default Header;