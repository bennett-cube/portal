import React, { Fragment, useEffect } from 'react';
import axios from 'axios';
import { useTranslation } from 'react-i18next';
import { Phone, Info } from 'react-feather';
import Header from '../header';
import SearchBar from '../searchbar';
import Map from '../map';
import InfoGraphic from '../../styles/images/infographic.svg';
import './styles.scss';

const LandingPage = () => {
    const { t } = useTranslation();

    // TODO: cleanup this
    useEffect(async () => {
        const result = await axios.get('http://www.mocky.io/v2/5e7796ac3300002b5809a028');
        console.log(result.data);
    }, []);

    return (
        <Fragment>
            <Header />
            <SearchBar />
            <Map />
            <header className="landingpage__header">
                <h1> {t('landingpage.title')} </h1>
            </header>
            <section className="section-one">
                <div className="intro-notes">
                    <p>
                        {t('landingpage.sectionone_p1')}
                        <br />
                        <br />
                        {t('landingpage.sectionone_p2')} <br />
                        <br />
                        <a href="https://corona-help.org/">corona-help.org </a>
                        {t('landingpage.sectionone_p3')}
                    </p>
                </div>
                <div className="boxes">
                    <div className="box hotline-box">
                        <h2>{t('landingpage.hotlineBoxTitle')} </h2>
                        <div className="hotline-in-box">
                            <Phone size="30" />
                            <p> coming soon! </p>
                        </div>
                    </div>
                    <div className="box help-box">
                        <h2> {t('landingpage.helpBoxTitle')} </h2>
                        <p>
                            {t('landingpage.helpBoxText')}
                        </p>
                        <br />
                        <button type="button" className="btn btn-help"> {' '} coming soon </button>
                    </div>
                </div>
            </section>
            <section className="section-two">
                <img src={InfoGraphic} width="80%" height="auto" alt="infographics" />
                <div className="info-infographic">
                    <Info size="20" />
                    <p>{t('landingpage.sectiontwo_info')}</p>
                </div>
            </section>
            <section className="section-three">
                <h1>{t('landingpage.sectionthree_title')}</h1><br />
                <p> {t('landingpage.sectionthree_alt')}</p>
            </section>
        </Fragment>
    );
};

export default LandingPage;
