import React, { useEffect } from 'react';
import 'leaflet/dist/leaflet.css';
import L from 'leaflet';
import { MAP_API_KEY } from '../../config';

delete L.Icon.Default.prototype._getIconUrl;
L.Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

const Map = () => {

    useEffect(() => {
        var mymap = L.map('mapid').setView([52.5127808, 13.3898995], 14);

        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1,
            accessToken: MAP_API_KEY
        }).addTo(mymap);

        //TODO get markers from API
        var marker = L.marker([52.5127808, 13.3898995]).addTo(mymap);
        marker.bindPopup("<b>Help needed!</b><br>Person in need 1");

        var marker2 = L.marker([52.5157808, 13.3866995]).addTo(mymap);
        marker2.bindPopup("<b>Help needed!</b><br>Person in need 2");

        var marker3 = L.marker([52.5097808, 13.3966995]).addTo(mymap);
        marker3.bindPopup("<b>Help needed!</b><br>Person in need 3");
    }, [])

    return (
        <div id="mapid" style={{ height: '400px' }}></div>
    )
}

export default Map;