import React from 'react';
import {
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Input,
    Button
} from 'reactstrap';
import { MapPin } from 'react-feather';
import { useTranslation } from 'react-i18next';
import './styles.scss';

// TODO These values must come from API
const FILTERS = [
    'Einkäufe',
    'Tierpflege',
    'Hilfe bei Online-Einkäufen',
    'Kinderbetreung'
];

const SearchBar = () => {
    const { t } = useTranslation();

    return (
        <div className="searchbar">
            <h3 className="searchbar__title">{t('searchbar.title')}</h3>
            <InputGroup className="searchbar__inputgroup">
                <InputGroupAddon addonType="prepend">
                    <InputGroupText className="searchbar__input-icon">
                        <MapPin size="20" />
                    </InputGroupText>
                </InputGroupAddon>
                <Input
                    className="searchbar__input"
                    placeholder={t('searchbar.inputPlaceholder')}
                />
            </InputGroup>
            <div className="searchbar__filter-buttons">
                {FILTERS.map((item, index) => (
                    <Button key={`filter_${index}`} > {item}</Button>
                ))}
            </div>
        </div >
    );
};

export default SearchBar;
