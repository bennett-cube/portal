# Corona-Help.org Volunteer Initiative Web Portal
# Copyright (C) 2020 Corona-Help.org Developers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView

from coronahelp.apps.registration.views import UserIsSmSverified


class LocalActivistsPage(UserIsSmSverified, TemplateView):
    '''
    View of the Volunteerlogged in area (maps etc.)
    '''

    template_name = 'public/landing_page.html'
