from django.conf import settings
from django.db.models.signals import pre_save
from django.dispatch import receiver
from opencage.geocoder import OpenCageGeocode

from coronahelp.apps.personinneed.models import PersonInNeed


@receiver(pre_save, sender=PersonInNeed)
def model_Person_in_need_pre_save(sender, **kwargs):
    ###
    # receive geo data before saving the instance
    ###
    if settings.TESTGEO or not settings.DEBUG:
        geocoder = OpenCageGeocode(settings.CAGE_GEOCODE_TOKEN)
        query = kwargs['instance'].street_name + " " + \
            kwargs['instance'].street_number + \
            str(kwargs['instance'].postal_code)
        results = geocoder.geocode(query)
        kwargs['instance'].lat = results[0]['geometry']['lat']
        kwargs['instance'].long = results[0]['geometry']['lng']
