# Corona-Help.org Volunteer Initiative Web Portal
# Copyright (C) 2020 Corona-Help.org Developers
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db.models.signals import pre_save
from django.dispatch import receiver
from opencage.geocoder import OpenCageGeocode
from .models import User
from django.conf import settings


@receiver(pre_save, sender=User)
def model_user_pre_save(sender, **kwargs):
    ###
    # receive geo data before saving the instance
    ###
    if not settings.DEBUG:
        geocoder = OpenCageGeocode(settings.CAGE_GEOCODE_TOKEN)
        query = kwargs['instance'].street_name+" " + \
            kwargs['instance'].street_number + \
            str(kwargs['instance'].postal_code)
        results = geocoder.geocode(query)
        kwargs['instance'].lat = results[0]['geometry']['lat']
        kwargs['instance'].long = results[0]['geometry']['lng']
